<?php

use \Timenz\Crud\Crud;

class MenuData{
    public $menu_master_blade;
    public $menu_route;

}

class MenuCrud extends Crud{

    public $viewData;

    private $masterData;
    private $view_path = 'packages.timenz.menu-manager.';

    public function __construct(){
        $this->masterData = new MenuData();

        $this->masterData->menu_master_blade = Config::get('menu-manager::master_blade');
        $this->masterData->menu_route = Config::get('menu-manager::route');

        $this->tableRole = 'web_role';
        $this->tableTopMenu = 'web_top_menu';
    }

    protected function run(){

        $this->masterData->row_role = DB::table($this->tableRole)->get();
        $title = 'menu Manager';
        $roleId = 1;
        if(Session::has('role_id')){
            $roleId = Session::get('role_id');
        }

        $rowRole = DB::table($this->tableRole)->find($roleId);

        if($rowRole == null){
            Session::forget('role_id');
            return false;
        }

        $this->masterData->role = $rowRole;

        $rowMenu = DB::table($this->tableTopMenu)->where(array('id_role' => $roleId))->orderBy('urutan', 'asc')->get();

        $menu = $this->orderMenu($rowMenu, 0);


        $this->masterData->menu = $menu;
        if(!isset($this->masterData->menu_id)){
            $this->masterData->menu_id = 0;
        }else{
            $rowParent = DB::table($this->tableTopMenu)->find($this->masterData->menu_id);
            $this->masterData->row_menu = [];
            if($rowParent !== null){
                $this->masterData->row_menu = $rowParent;
                $title = 'Sub of '.$rowParent->title;
            }

        }


        $this->init('web_top_menu');
        $this->where('t0.is_visible', '=', 'Y');
        $this->orderBy('urutan', 'asc');
        $this->setJoin('id_role', 'web_role', 'role');

        $option = array();

        foreach($rowMenu as $item){
            $option[$item->id] = $item->title;
        }

        $this->changeType('id_role', 'hidden', $roleId);
        $this->changeType('is_visible', 'hidden', 'Y');
        $this->changeType('parent_route', 'hidden', 0);
        if(Session::has('parent')){
            $parentId = Session::get('parent');
            if($parentId > 0){
                $this->changeType('parent_route', 'hidden', $parentId);
            }
        }

        $this->title = $title;
        $this->columns(array(
            'title',
            'route',
            'urutan',
        ));
        $this->fields(array(
            'id_role',
            'parent_route',
            'title',
            'route',
            'urutan',
            'is_visible',
            'is_active',
        ));

        $this->allowDelete = false;
        $this->allowRead = false;
        $this->allowSearch = false;


        $this->setMasterBlade($this->view_path.'menu');


        $this->viewData['menu'] = $this->masterData;


        $this->setMasterData($this->viewData);
        return true;
    }

    private function orderMenu($menu, $parent){
        $out = [];
        foreach($menu as $item){
            if($item->parent_route != $parent){
                continue;
            }

            $out[] = [
                'id' => $item->id,
                'parent_route' => $item->parent_route,
                'route' => $item->route,
                'title' => $item->title,
                'urutan' => $item->urutan,
                'is_active' => $item->is_active,
                'child' => $this->orderMenu($menu, $item->id)
            ];
        }

        return $out;
    }



    public function index(){
        if(!Session::has('role_id') ){
            $role = DB::table($this->tableRole)->first();
            //$role = array();
            if($role == null){
                return 'Tabel role kosong';
            }

            $role_id = $role->id;
            Session::put('role_id', $role_id);

            return Redirect::to($this->masterData->menu_route);
        }
        $role_id = Session::get('role_id');
        $this->where('t0.id_role', '=', $role_id);

        $parent = 0;

        if(Session::has('parent')){
            $parent = Session::get('parent');
        }else{
            Session::put('parent', $parent);
        }

        $this->masterData->menu_id = $parent;

        $this->where('t0.parent_route', '=', $parent);

        return parent::index();
    }

    public function create(){
        $this->changeType('is_active', 'hidden', 'Y');
        return parent::create();
    }
}