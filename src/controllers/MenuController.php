<?php

use Illuminate\Routing\Controller;

class MenuController extends Controller{

    private $masterData;
    private $tableRole;
    private $tableTopMenu;

    public function __construct(){
        $this->masterData = new MenuData();
        $this->tableRole = 'web_role';
        $this->tableTopMenu = 'web_top_menu';

        $this->masterData->menu_route = Config::get('menu-manager::route');
    }

    public function goChild($id){
        Session::put('parent', $id);
        return Redirect::to($this->masterData->menu_route);
    }

    public function goRole($id){
        Session::put('role_id', $id);
        Session::forget('parent');
        return Redirect::to($this->masterData->menu_route);
    }

    public function addRole(){
        $role = trim(Input::get('role'));

        if($role == ''){
            return Redirect::to($this->masterData->menu_route);
        }

        $rows = DB::table($this->tableRole)->where('role', '=', $role)->first();

        if($rows != null){
            return Redirect::to($this->masterData->menu_route);
        }

        DB::table($this->tableRole)->insert(array(
            'role' => $role
        ));

        return Redirect::to($this->masterData->menu_route);
    }

    public function delRole(){
        $role = trim(Input::get('role_id'));

        if($role == ''){
            return Redirect::to($this->masterData->menu_route);
        }

        Session::forget('role_id');

        DB::table($this->tableRole)->delete($role);
        DB::table($this->tableTopMenu)->where('id_role', '=', $role)->delete();

        return Redirect::to($this->masterData->menu_route);
    }

    public function delMenu(){
        $menuId = trim(Input::get('menu_id'));

        if($menuId == ''){
            return Redirect::to($this->masterData->menu_route);
        }

        Session::forget('parent');

        DB::table($this->tableTopMenu)->delete($menuId);
        DB::table($this->tableTopMenu)->where('parent_route', '=', $menuId)->delete();

        return Redirect::to($this->masterData->menu_route);
    }

    public function copyMenu(){
        $menuId = trim(Input::get('menu_id'));
        $menuTitle = trim(Input::get('menu_title'));
        $urutan = trim(Input::get('urutan'));
        $targetRoleId = trim(Input::get('target_role'));

        if($menuId == '' or $targetRoleId == '' or $menuTitle == '' or $urutan == ''){
            return Redirect::to($this->masterData->menu_route);
        }

        $cek = DB::table($this->tableTopMenu)->where(['title' => $menuTitle, 'id_role' => $targetRoleId])->first();

        if($cek !== null){
            return Redirect::to($this->masterData->menu_route);

        }

        $this->recurInsert($menuId, $targetRoleId);


        Session::put('role_id', $targetRoleId);
        return Redirect::to($this->masterData->menu_route);
    }

    private function recurInsert($menuId, $targetRoleId, $parent = 0){
        if($parent == 0){
            $rows = DB::table($this->tableTopMenu)->where('id', '=', $menuId)->get();
        }else{
            $rows = DB::table($this->tableTopMenu)->where('parent_route', '=', $menuId)->get();
        }



        if($rows !== null){
            foreach($rows as $row){
                $insert = [
                    'id_role' => $targetRoleId,
                    'parent_route' => $parent,
                    'route' => $row->route,
                    'title' => $row->title,
                    'urutan' => $row->urutan,
                    'is_visible' => 'Y',
                    'is_active' => 'Y',
                ];

                $id = DB::table($this->tableTopMenu)->insertGetId($insert);
                $this->recurInsert($row->id, $targetRoleId, $id);
            }


        }

    }
}