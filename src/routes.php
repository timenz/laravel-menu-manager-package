<?php

$route = Config::get('menu-manager::route');

//Route::resource($route, 'MenuCrud');

Route::get($route.'/go_child/{id}', array('uses' => 'MenuController@goChild'));
Route::get($route.'/go_role/{id}', array('uses' => 'MenuController@goRole'));

Route::post($route.'/add_role', array('uses' => 'MenuController@addRole', 'before' => 'csrf'));
Route::post($route.'/del_role', array('uses' => 'MenuController@delRole', 'before' => 'csrf'));
Route::post($route.'/del_menu', array('uses' => 'MenuController@delMenu', 'before' => 'csrf'));
Route::post($route.'/copy_menu', array('uses' => 'MenuController@copyMenu', 'before' => 'csrf'));