@extends($menu->menu_master_blade)

@section('konten')
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-success">

                <div class="panel-body">
                    <div class="row">



                        <div class="col-md-7">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Role Option</h3>
                                        </div>
                                        <div class="panel-body">
                                            <div  style="margin-bottom: 15px;">
                                                <ul class="nav nav-pills">

                                                    @foreach($menu->row_role as $item)
                                                        <?php $class = '';
                                                        if($menu->role->id == $item->id){
                                                            $class = 'active';
                                                        }
                                                        ?>
                                                        <li class="{{ $class }}"><a href="{{ url($menu->menu_route.'/go_role/'.$item->id) }}">{{ $item->role }}</a></li>
                                                    @endforeach
                                                </ul>
                                            </div>


                                            <form class="form-inline" action="{{ url($menu->menu_route.'/add_role') }}" method="post">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <p>Tambah Role
                                                    <input type="text" class="form-control" name="role"> ?
                                                    <button class="btn btn-success">GO</button></p>
                                            </form>


                                            <form class="form-inline form-del" action="{{ url($menu->menu_route.'/del_role') }}" method="post">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <input type="hidden" value="{{ $menu->role->id }}" name="role_id">
                                                <p>Delete role <strong>{{ $menu->role->role }}</strong> and it's menu ?
                                                    <button class="btn btn-danger del-button">GO</button></p>
                                            </form>
                                        </div>
                                    </div>


                                </div>



                                @if(isset($menu->row_menu->id))

                                    <div class="col-md-12">
                                        <div class="panel panel-success">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">Top Menu Option</h3>
                                            </div>
                                            <div class="panel-body">
                                                @if($menu->row_menu->parent_route == 0)
                                                    <form class="form-inline" action="{{ url($menu->menu_route.'/copy_menu') }}" method="post">
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                        <input type="hidden" value="{{ $menu->row_menu->id }}" name="menu_id">
                                                        <input type="hidden" value="{{ $menu->row_menu->title }}" name="menu_title">
                                                        <input type="hidden" value="{{ $menu->row_menu->route }}" name="route">
                                                        <input type="hidden" value="{{ $menu->row_menu->urutan }}" name="urutan">
                                                        <p>Copy menu <strong>{{ $menu->row_menu->title }}</strong> and it's sub-menu to
                                                            <select class="form-control" name="target_role">
                                                                @foreach($menu->row_role as $item)
                                                                    <?php $class = '';
                                                                    if($menu->role->id == $item->id){
                                                                        continue;
                                                                    }
                                                                    ?>
                                                                    <option value="{{ $item->id }}">{{ $item->role }}</option>
                                                                @endforeach
                                                            </select> ?
                                                            <button class="btn btn-success">GO</button></p>
                                                    </form>
                                                @endif


                                                <form class="form-inline form-del" action="{{ url($menu->menu_route.'/del_menu') }}" method="post">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <input type="hidden" value="{{ $menu->row_menu->id }}" name="menu_id">
                                                    <p>Delete menu <strong>{{ $menu->row_menu->title }}</strong> and it's sub-menu ?
                                                        <button class="btn btn-danger del-button">GO</button></p>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                @endif


                                <div class="col-md-12">
                                    @if(Session::has('messages'))
                                        <div class="">
                                            <div class="alert alert-dismissable alert-success col-md-12">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                <strong>Selamat !!!</strong> {{ Session::get('messages') }}
                                            </div>
                                        </div>

                                    @endif
                                    @yield('konten')
                                </div>
                            </div>
                        </div>


                        <div class="col-md-5">
                            <h4>Menu Tree of role {{ $menu->role->role }}</h4>
                            <?php
                            $menuId = 0;
                            if(isset($menu->row_menu->id)){$menuId = $menu->row_menu->id; }

                            ?>

                            @if(count($menu->menu) > 0){{ recurMenu($menu->menu, $menuId, $menu->menu_route) }}@endif
                            <?php

                            function recurMenu($menu, $menuId, $menu_route){
                                if(count($menu) < 1){
                                    return '';
                                }

                                $str = '';


                                foreach($menu as $item){
                                    if($item['id'] == $menuId){
                                        $link = '<strong>'.$item['title'].'</strong>';
                                    }else{
                                        $link = '<a href="'.url($menu_route.'/go_child/'.$item['id']).'">'.$item['title'].'</a>';
                                    }


                                    $str .= '<ul>';
                                    $str .= '<li>'.$link.recurMenu($item['child'], $menuId, $menu_route).'</li>';
                                    $str .= '</ul>';
                                }
                                return $str;
                            }
                            ?>

                        </div>


                    </div>


                </div>
            </div>





        </div>


    </div>


@overwrite

@endsection

@section('js')
<script>
    $(function(){
        $('.del-button').click(function(e){
            e.preventDefault();
            var konfirm = confirm('Yakin untuk mendelete ?');
            if(konfirm){
                $('.form-del').submit();
            }
        });
    });
</script>
@yield('js')
@overwrite
@endsection