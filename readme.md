## Halaman Menu Manager

Untuk me-manage user role dan hak aksesnya.

### Install
- Add to config/app providers
    ```
    'Timenz\MenuManager\MenuManagerServiceProvider',
    ```
    
- Publish View
    ```
    php artisan view:publish timenz/menu-manager
    // on workbench
    php artisan view:publish --path="workbench/timenz/menu-manager/src/views" timenz/menu-manager
    ```
    
- Config View
    ```
    php artisan config:publish timenz/menu-manager
    // on workbench
    php artisan config:publish --path="workbench/timenz/menu-manager/src/config" timenz/menu-manager
    ```
    
- Add Route
    ```
    Route::resource('menu', 'MenuCrud');
    ```
    
    notes: route name sama config 'route' isinya harus sama
    
- Create Controller

    ```
    <?php
    
    class MenuManager extends MenuCrud{
    
        public function __construct(){
            parent::__construct();
            $viewData = ['description' => 'test'];
            $this->viewData = $viewData;
        }
    }
    ```

### Dependensi
- laravel
- timenz/crud
- bootstrap

